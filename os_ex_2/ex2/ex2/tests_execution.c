#define _CRT_SECURE_NO_WARNINGS

#define ERROR_CODE_THREAD_CREATION "Error occured during thread creation, exiting.\n"

#include <stdbool.h>
#include <Windows.h>
#include <tchar.h>
#include "test_execution.h"
#include "tests_and_args.h"

DWORD WINAPI OpenProcessForEXE(LPVOID lpParam) {
	Test *MyTest;
	char BUFFER[MAX_CHARS_IN_LINE];
	PROCESS_INFORMATION MyProcInf;
	MyTest = (Test*)lpParam;

	//strcat(BUFFER, MyTest->path_to_exe);
	/*
	for (int i = 0; i < MyTest->amount_of_args; i++) {
		strcat(BUFFER, " ");
		strcat(BUFFER, (MyTest->args)[i]);
	} */
	printf("%s\n", MyTest->path_to_exe); 
	//printf("you fucking piece of shit %d", GetThreadId());
	//CreateProcess(NULL, BUFFER, NULL, NULL, FALSE, NORMAL_PRIORITY_CLASS, NULL, NULL, NULL, &MyProcInf);
	
	return 0;
}

static HANDLE CreateThreadSimple(LPTHREAD_START_ROUTINE p_start_routine,Test *test2execute, LPDWORD p_thread_id) {
	HANDLE thread_handle;
	if (NULL == p_start_routine || NULL == p_thread_id) {
		printf(ERROR_CODE_THREAD_CREATION);
		exit(-1);
	}

	thread_handle = CreateThread(
		NULL,
		0,
		p_start_routine,
		test2execute,           //lpParameters
		0,						//creation flags
		p_thread_id);

	if (NULL == thread_handle) {
		printf(ERROR_CODE_THREAD_CREATION);
		exit(-1);
	}
	return thread_handle;
}
int CountTests(Test *TestList) {
	int res = 0;
	Test *current_test = TestList;
	while (NULL != current_test) {
		res++;
		current_test = current_test->Next;
	}
	return res;
}

DWORD open_threads_for_tests(Test *TestList) { // this function will opn a thread for each test and wait till all threads are closed
	
	Test *current_test = TestList;
	int TestCount = CountTests(TestList);
	int i = 0;
	HANDLE Tests_handles[2];
	DWORD Thread_ids[2];
	DWORD MyResult;
	DWORD dwTestCount = (DWORD)TestCount;

	for (i = 0; i < 2; i++) {
		Tests_handles[i] = CreateThreadSimple(OpenProcessForEXE, current_test, &Thread_ids[i]);
		current_test = current_test->Next;
	}
	MyResult = WaitForMultipleObjects(dwTestCount, Tests_handles, TRUE, INFINITE);
	return MyResult;
}