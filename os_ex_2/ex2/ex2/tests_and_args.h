#pragma once


#define MAX_CHARS_IN_LINE 101 
#define MEMORY_ALLOCATION_ERR "Error: Memory allocation failed, exiting...\n"
#define FILE_HANDLING_ERR "Error occured in file handling, exiting...\n"

typedef struct _Test {
	char *path_to_exe; 
	char **args; 
	char *path_to_res;
	int amount_of_args;
	int status; 
	struct _Test *Next; 
}Test;

typedef struct _LinePart { // This struct is used to parse paths and arguments from input files 
	char *part; 
	struct _LinePart *Next; 
}LinePart;

int creat_test(Test *test); 

void delete_test(Test *test); 

void delete_tests_list(Test **TestList); 

void trim_newline(char *line); 

void delete_LineList(LinePart *LineList); 

void delete_args(char **args, int amount); 

int parse_line_to_test(char *line, char **path_to_exe, char ***args, char **path_to_res); 

int creat_TestList(Test **TestList, char *TestFilePath); 