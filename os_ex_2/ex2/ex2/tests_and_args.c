#define _CRT_SECURE_NO_WARNINGS

#include <stdio.h>
#include <string.h> 
#include "tests_and_args.h"


void delete_test(Test *test) {
	if (NULL == test) {
		return; 
	}
	free(test->path_to_exe); 
	free(test->path_to_res); 
	delete_args(test->args, test->amount_of_args); 
	free(test); 
	return; 
}

void delete_tests_list(Test **TestList) {
	Test *current_test = *TestList; 
	int i = 0;
	while (NULL != *TestList) {
		current_test = *TestList; 
		*TestList = (*TestList)->Next;
		delete_test(current_test); 
	}
}

void trim_newline(char *line) {
	char *service_ptr = line; 
	while (*service_ptr != '\n') {
		service_ptr++; 
	}
	*service_ptr = '\0'; 
	return;
}

void delete_LineList(LinePart *LineList) { // Complete this function 
	LinePart *current_part = LineList;  
	while (NULL != current_part) {
		free(current_part->part);
		current_part = current_part->Next; 
	}
}

void delete_args (char **args, int amount) {
	int i = 0; 
	for (i = 0; i < amount; i++) {
		free(args[i]); 
	}
	free(args); 
}

int parse_line_to_test (char *line, char **path_to_exe, char ***args, char **path_to_res, int* amount_of_args) {
	int part_counter = 0; 
	int arg = 0; // Idx will be used to go over all the arguments in the line in a for loop 
	char *token = NULL; // Will be used with strtok to parse the line 
	LinePart *LineList = malloc((int)sizeof(LinePart)); 
	LinePart *current_part = LineList; 
	if (NULL == LineList) {
		printf(MEMORY_ALLOCATION_ERR); 
		return -1; 
	}
	trim_newline(line); 
	token = strtok(line, " "); 
	while (1) {
		current_part->part = malloc((strlen(token) + 1) * sizeof(char)); 
		if (NULL == current_part->part) {
			printf(MEMORY_ALLOCATION_ERR);
			delete_LineList(LineList); 
			return -1; 
		}
		strcpy(current_part->part,token); 
		token = strtok(NULL, " "); // Move on to next word in line
		if (NULL == token) {
			current_part->Next = NULL; 
			break; 
		}
		current_part->Next = malloc((int)sizeof(LinePart)); 
		if (NULL == current_part->Next) {
			printf(MEMORY_ALLOCATION_ERR); 
			delete_LineList(LineList);
			return -1;
		}
		current_part = current_part->Next; 	 
	}
	// Finished parsing the line to a list, now update the variables of the tests accordingly 

	current_part = LineList;
	//Count how many parts the line contains
	while (NULL != current_part) {
		part_counter++; 
		current_part = current_part->Next; 
	} // End of part counnting
	*args = calloc(part_counter-2, sizeof(char*)); 
	if (NULL == *args) {
		printf(MEMORY_ALLOCATION_ERR); 
		delete_LineList(LineList); 
		return -1; 
	}
	*amount_of_args = part_counter - 2;
	current_part = LineList; 
	*path_to_exe = malloc((strlen(current_part->part)+1)*sizeof(char)); 
	if (NULL == *path_to_exe) {
		printf(MEMORY_ALLOCATION_ERR); 
		delete_LineList(LineList); 
		return -1; 
	}
	strcpy(*path_to_exe, current_part->part); 
	current_part = current_part->Next; 
	for (arg = 0; arg < part_counter - 2; arg ++) {
		(*args)[arg] = malloc((strlen(current_part->part)+1)*sizeof(char));
		if (NULL == (*args)[arg]) {
			printf(MEMORY_ALLOCATION_ERR); 
			delete_LineList(LineList); 
			delete_args(*args, part_counter - 2); 
			return -1; 
		}
		strcpy((*args)[arg], current_part->part); 
		current_part = current_part->Next; 
	}
	// The last LinePart is the *path_to_res 
	*path_to_res =(char*) malloc((strlen(current_part->part) + 1) * sizeof(char));
	if (NULL == *path_to_exe) {
		printf(MEMORY_ALLOCATION_ERR);
		delete_LineList(LineList);
		return -1;
	}
	strcpy(*path_to_res, current_part->part); 
	return 0; 
}

int creat_TestList(Test **TestList, char *TestFilePath) { // this function is not finished
	Test *current_test = *TestList; 
	Test *prev_test = NULL;
	FILE *fptr = NULL;
	char current_line[MAX_CHARS_IN_LINE]; 
	int check_operation = 0;  
	fptr = fopen(TestFilePath, "r"); 
	if (NULL == fptr) { // Error in file opening 
		printf(FILE_HANDLING_ERR); 
		return -1; 
	}
	// Read each line from input file and creat a test for it
	while (NULL != fgets(current_line, MAX_CHARS_IN_LINE, fptr)) {

		current_test = malloc((int)sizeof(Test));
		if (NULL == current_test) {
			printf(MEMORY_ALLOCATION_ERR);
			fclose(fptr);
			delete_tests_list(&TestList);
			return -1;
		}
		if (NULL != prev_test) {
			prev_test->Next = current_test;
		}
		else
		{
			*TestList = current_test;
		}
		check_operation = parse_line_to_test(current_line, &(current_test->path_to_exe), &(current_test->args), &(current_test->path_to_res), &(current_test->amount_of_args));
		if (check_operation < 0) {
			fclose(fptr);
			delete_tests_list(TestList); 
			return -1; 
		}

		prev_test = current_test;
	}
	current_test->Next = NULL;
	fclose(fptr);
	if (NULL != *TestList)return 0; else return -1;
}